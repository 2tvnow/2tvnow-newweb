<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <title>2TVNow</title>
    <%@ include file="head.jsp" %>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-52625152-1', 'auto',{'name': 'news'});
        ga('news.send', 'pageview');

    </script>

</head>
<body>
<div class="header">
    <%@ include file="menu.jsp" %>
    <div id="small-dialog" class="mfp-hide small-dialog">
        <div class="pop_up">
            <h3>前往報名</h3>

            <p>趕快到官網報名參加吧！</p>
            <ul>
                <li><a class="ok" href="http://www.accupass.com/go/0822"> </a></li>
            </ul>
        </div>
    </div>

    <div id="small-dialog2" class="mfp-hide small-dialog">
        <div class="pop_up">
            <h3>前往報名</h3>

            <p>趕快到官網報名參加吧！</p>
            <ul>
                <li><a class="ok" href="http://www.dtvc.org.tw/event/2014smarttv/presentation.php?id=3"> </a></li>
            </ul>
        </div>
    </div>

    <div id="small-dialog3" class="mfp-hide small-dialog">
        <div class="pop_up">
            <h3>前往報名</h3>

            <p>趕快到官網報名參加吧！</p>
            <ul>
                <li><a class="ok" href="http://www.accupass.com/go/iiiapp"> </a></li>
            </ul>
        </div>
    </div>

    <div id="small-dialog4" class="mfp-hide small-dialog">
        <div class="pop_up">
            <h3>前往報名</h3>

            <p>趕快到官網報名參加吧！</p>
            <ul>
                <li><a class="ok" href="http://www.accupass.com/go/micapp"> </a></li>
            </ul>
        </div>
    </div>

    <div id="small-dialog5" class="mfp-hide small-dialog">
        <div class="pop_up">
            <h3>前往報名</h3>

            <p>趕快到官網報名參加吧！</p>
            <ul>
                <li><a class="ok" href="http://www.dtvc.org.tw/event/2015smarttv/Active/Index/6"> </a></li>
            </ul>
        </div>
    </div>


    <div id="small-dialog6" class="mfp-hide small-dialog">
        <div class="pop_up">
            <h3>前往報名</h3>

            <p>趕快到官網報名參加吧！</p>
            <ul>
                <li><a class="ok" href="http://www.dtvc.org.tw/event/2015smarttv/Active/Index/1009"> </a></li>
            </ul>
        </div>
    </div>

</div>
<!---//End-get-download-link---->
<!----start-divice-features---->
<div class="single-page">

    <div class="row box-white">
        <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
            <div class="col-md-4 col-xs-12">
                <img src="images/2015conference.jpg">
            </div>
            <div class="col-md-8 col-xs-12">
                <h3>2015年跨螢互動暨新媒體趨勢研討會</h3>

                <p>
                    隨著行動裝置的迅速普及，電視產業在數位化後展開全新的紀元。
                    資、通訊與網路的結合，使電視與各種手持式裝置，有了不一樣的關係。
                    資策會智通所謹訂於中華民國104年10月14日(星期三)下午13點20分，假集思交通部國際會議中心3+4樓國際會議廳，
                    舉行「跨螢互動暨新媒體趨勢研討會」，邀請業界專家分享跨螢互動應用發展趨勢<br>
                    <br>
                    ● 時間：2015年10月15日 13:20 ~ 2015年10月15日 17:10<br>
                    ● 地點：集思交通部國際會議中心3-4樓國際會議廳<br>
                    <br>
                </p>
                <a class="bigbtn popup-with-zoom-anim" href="#small-dialog6" href="#">報名</a>
            </div>
        </div>
    </div>

    <div class="row box-gray">
        <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
            <div class="col-md-8 col-xs-12">
                <h3>2015年7月-第二螢智慧電視產業論壇交流會</h3>

                <p>
                    在數位匯流及三網融合時代，隨著網際網路的盛行及終端設備的發展，
                    消費者能夠隨時隨地進行閱讀，收視，娛樂，及學習等活動。影音內容
                    透過網路的串連，更讓這些活動中最生動、最吸引人的部份被記憶與分
                    享。新媒體時代，消費者獲取資訊的行為大幅改變。

                    7月份第二螢智慧電視論壇邀請2011年數位時代的創業之星—果實伙
                    伴(股)公司創辦人暨總經理葉毓輝先生，分享數位影音廣告如何與電視
                    互補及與消費者有效溝通的策略。

                    跨平台直播市場在新媒體市場愈來愈受矚目，紅心辣椒與韓國最紅直播
                    平台AfreecaTV合作，使得辣椒艾菲卡TV正式開台，備受觀注與討論，
                    本月份邀請辣椒艾菲卡直播(股)公司李亦華副總經理針對線上即時互動的
                    魅力及直播內容與案例分享。演講主題為「LIVE新魅力_自由.隨意.互動
                    直播平台」 。<br><br>

                    ● 時間：2015年7月28日(二)下午2:00--4:00<br>
                    ● 地點：資策會民生大樓14樓關懷廳 (北市民生東路4段133號14樓)<br><br>
                </p>
                <a class="bigbtn popup-with-zoom-anim" href="#small-dialog5" href="#">報名</a>
            </div>
            <div class="col-md-4 col-xs-12">
                <br><br><br><br>
                <img src="images/201507.jpg">
            </div>
        </div>
    </div>

    <div class="row box-white">
        <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
            <div class="col-md-4 col-xs-12">
                <br><br><br><br>
                <img src="http://accupassv3storage.blob.core.windows.net/userupload/051c51df85d34ee08dcafbdd438ff645.jpg">
            </div>
            <div class="col-md-8 col-xs-12">
                <h3>第二螢APP設計與企劃實戰工作坊</h3>

                <p>
                    隨著行動裝置的普及，愈來愈多人習慣一邊參觀博物館、美術館或看電視時，一邊使用智慧手機或平板，做為第二個螢幕來獲取更多想要的資訊。
                    國內外許多著名影視內容也都使用第二螢的互動APP創造更多互動式體驗，包括熱門影集陰屍路使用第二螢和節目內容互動，
                    預測主角使用武器及擊斃殭屍隻數等互動應用；或者是海尼根結合4G網路開發Heineken Star Player，讓消費者觀看足球賽直播時，
                    亦可利用App進行即時互動，藉此創造品牌曝光度與知名度等。這些第二螢相關的應用程式，已經提供了更豐富的收視體驗，也創造出新的商業模式。<br>
                    <br>
                    ● 時間：2015年5月26日 (二) 9:00-17:30<br>
                    ● 地點：資策會 MIC 台北市大安區敦化南路二段216號23樓（文創沙龍）<br>
                    <br>
                </p>
                <a class="bigbtn popup-with-zoom-anim" href="#small-dialog4" href="#">報名</a>
            </div>
        </div>
    </div>


    <div class="row box-gray">
        <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
            <div class="col-md-8 col-xs-12">
                <h3>第二螢互動式APP行銷設計工作坊</h3>

                <p>
                    隨著行動裝置的普及，愈來愈多人習慣一邊看電視，一邊使用智慧手機或平板，做為第二個螢幕，從事各項活動。
                    國內外許多著名影視內容也都使用第二螢的互動APP創造更多互動式體驗，包括熱門影集陰屍路使用第二螢和節目內容互動，
                    預測主角使用武器及擊斃殭屍隻數等互動應用；或者是海尼根結合4G網路開發Heineken Star Player，讓消費者觀看足球賽直播時，
                    亦可利用App進行即時互動，藉此創造品牌曝光度與知名度等。這些第二螢相關的應用程式，已經提供了更豐富的收視體驗，也創造出新的商業模式。
                    國內的文創影視業者也已經察覺這個趨勢，但對於如何設計出一個符合消費者使用習慣，同時可以創造一源多用機會的第二螢APP，還是有許多疑問。
                    因此本次特別邀請資策會智慧網通系統研究所劉文山主任、使用者體驗設計專家悠識數位的首席體驗架構師蔡明哲老師，
                    以及全台最多人使用的第二螢應用程式Timely.tv的產品負責人曾友志，所組成的超強團隊來分享，
                    如何企劃與設計一個與文創影視內容互動的第二螢應用程式，也會分享國內外經典第二螢APP案例供學員參考。<br>
                    ● 時間：2014年12月9日 (二) 08:30-17:00<br>
                    ● 地點：資策會 MIC 台北市大安區敦化南路二段216號23樓（文創沙龍）<br><br>
                </p>
                <a class="bigbtn popup-with-zoom-anim" href="#small-dialog3" href="#">報名</a>
            </div>
            <div class="col-md-4 col-xs-12">
                <img src="images/2screen-3.png">
            </div>
        </div>
    </div>

    <div class="row box-white">
        <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
            <div class="col-md-4 col-xs-12">
                <br><br><br>
                <img src="images/2screen-2.png">
                <br>
            </div>
            <div class="col-md-8 col-xs-12">
                <h3>全國大專院校智慧電視應用服務創意競賽</h3>

                <p>
                    由經濟部技術處指導，資策會智通所主辦的「2014全國大專院校智慧電視應用服務創意競賽」，期望能發掘各種智慧電視與行動裝置之互動及創新應用，
                    如包含社交網絡服務、個人或家庭遊戲及Web 3.0等雲端服務應用概念。
                    期望鼓勵大專院校學生發揮創意，在數位匯流的時代，想像現今民眾生活中收看電視的創新應用模式，創造具實用性及可行性的方案。<br>
                    <br>
                    ● 時間：103年11月13日 13:30~18:00<br>
                    ● 地點：台大醫院國際會議中心301會議室 台北市徐州路2號3樓<br>
                    <br>
                </p>
                <a class="bigbtn popup-with-zoom-anim" href="#small-dialog2" href="#">報名</a>
            </div>
        </div>
    </div>


    <div class="row box-gray">
        <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
            <div class="col-md-8 col-xs-12">
                <h3>第二螢智慧電視產業論壇成果分享研討會</h3>

                <p>
                    這次研討會邀請到講師都是業界重量級的專家，進行精彩的案例分享<br>
                    活動現場也邀請國內技術領先業者進行第二螢智慧電視應用服務的展示<br>
                    有多樣應用服務都是國內第一次發表，千萬不要錯過這個難得的機會<br><br>
                    ● 時間：103年11月13日上午08:40~12:30<br>
                    ● 地點：台大醫院國際會議中心301會議室 台北市徐州路2號3樓<br><br>
                </p>
                <a class="bigbtn popup-with-zoom-anim" href="#small-dialog" href="#">報名</a>
                </p>
            </div>
            <div class="col-md-4 col-xs-12">
                <img src="images/2screen-1.png">
            </div>
        </div>
    </div>
</div>
<!----//End-divice-features---->
<!---start-footer---->
<%@ include file="footer.jsp" %>
<!---//End-footer---->
<!--//End-content--->
<!----//End-wrap---->
</body>
</html>

