<link rel="stylesheet" href="test.css">
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <title>test</title>
</head>
<body>

<ul class="creatures" id="creatures">
    <li>
        <div class="click-anim">
            <figure class="hyper" style="background-color:#4ae9b8">
                <div class="eye"></div>
                <div class="eye"></div>
                <div class="mouth"></div>
            </figure>
        </div>
        <div class="shadow"></div>
    </li>

    <li class="nervous">
        <div class="click-anim">
            <figure style="background-color:mediumpurple">
                <div class="eye"></div>
                <div class="eye"></div>
                <div class="mouth teeth"></div>
            </figure>
        </div>
        <div class="shadow"></div>
    </li>

    <li>
        <div class="click-anim">
            <figure class="hungry" style="background-color:tomato">
                <div class="eye"></div>
                <div class="eye"></div>
                <div class="mouth"></div>
            </figure>
        </div>
        <div class="shadow"></div>
    </li>

    <li>
        <div class="click-anim">
            <figure style="background-color:limegreen">
                <div class="eye"></div>
                <div class="eye"></div>
                <div class="moustache"></div>
                <div class="mouth tooth"></div>
            </figure>
        </div>
        <div class="shadow"></div>
    </li>
</ul>
<h1><a href="#" target="_blank">喵嗚~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~</a></h1>

</body>
</html>
