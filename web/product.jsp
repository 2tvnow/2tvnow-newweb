<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <title>2TVNow</title>
    <%@ include file="head.jsp" %>
    <script src="js/page/acr-product.js"></script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-52625152-1', 'auto',{'name': 'product'});
        ga('product.send', 'pageview');

    </script>

</head>
<body>
<div ng-app="productApp" ng-controller="productCtrl" ng-init="initData()">
    <div class="header">
        <%@ include file="menu.jsp" %>
        <div class="modal fade" id="acr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
             style="top: 0" data-keyboard="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title">
                            ACR監測頻道(更新時間：2015.07.28)
                        </h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered table-hover table-striped">
                            <th width="10%" style="text-align:center">編號</th>
                            <th width="90%" style="text-align:center">頻道名稱</th>
                            <tr ng-repeat="data in tableData.results" class="ng-scope">
                                <td style="text-align:center">{{data.No}}</td>
                                <td>{{data.Names}}</td>
                            </tr>
                            </th>
                        </table>
                    </div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>

        <!---//End-get-download-link---->
        <!----start-divice-features---->
        <div class="single-page">
            <div class="row box-gray">
                <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
                    <div class="col-md-8 col-xs-12">
                        <h3>ACR自動內容辨識</h3>

                        <p>
                        第二螢應用中，最大的課題就是如何與電視內容連動，ACR辨識的功能可以使觀眾與電視產生互動，提供終端裝置辨識第一螢幕正在播出的影音內容，讓第二螢裝置與電視達成互動。<br><br>
                        ACR現有應用有兩種：<br>
                        1.頻道辨識：第二螢裝置可連線ACR服務平台，辨識目前觀看的頻道與節目<br>
                        2.特殊片段辨識：第二螢裝置可預存600秒以內的內容(如:20則30秒的廣告)，辨識特定內容<br>
                        <br>
                        ACR特色：<br>
                        ● 可支援20萬個以上的用戶同時進行辨識<br>
                        ● 不論是頻道辨識或是特殊片段辨識ACR都能在5秒鐘內準確的完成辨識<br>
                        ● 實測客廳噪音環境辨識率高達97%<br><br>
                        </p>
                        <!-- Indicates a dangerous or potentially negative action -->
                       <a class="btn btn-danger pull-right" data-toggle="modal" data-target="#acr">目前監控頻道</a>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <video class="videostyle" controls>
                            <source src="http://203.74.1.177/ACRChannelDemo.mp4" type="video/mp4">
                        </video>
                    </div>
                </div>
            </div>

            <div class="row box-white">
                <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
                    <div class="col-md-4 col-xs-12">
                        <img src="images/push-1.png">
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <h3>即時訊息推播服務Push Server</h3>

                        <p>
                        透過Push Server，你可以和使用者隨時保有最直接、最即時的緊密互動<br>
                        不論是發布活動或是行銷商品都可以非常迅速且準確的通知使用者<br><br>
                        2TV Push功能：<br>
                        ● Campaign Builder 後台管理：良好的後台介面，操作簡單易上手，輕鬆創建和解決複雜的推播<br>
                        ● Notification 訊息通知：在未開啟APP的狀態下，2TV Push還是可以將需要的訊息推播到用戶的手機中，快速又確實<br>
                        ● In-APP Message 圖文推播：在APP執行的過程中，2TV Push可以發送任何格式的資訊到用戶的手機中<br>
                        ● SDKs 軟體開發資源：包含ios和Android系統的開發工具包 <br>
                        ● Analytics & Reporting 分析&報告：豐富的分析功能，可以了解用戶對推播的參與度<br>
                        </p>
                    </div>
                </div>
            </div>

            <div class="row box-gray">
                <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
                    <div class="col-md-8 col-xs-12">
                        <h3>第二螢App產製服務</h3>

                        <p>
                        為了推展第二螢App，本團隊依過往製作相關App的經驗，以及市面上的第二螢App配合本團隊現有相關技術，整理成「第二螢應用程式開發模組」，以加快第二螢App的開發時程。
                        過去廠商若是要產製APP需要耗費非常多時間，有了APP產製服務的模組， 可以大幅縮短製作時間，且完全客製化，可以依照廠商的需求做調整。<br><br>
                        ● 使用者介面：主選單、圖文列表、影片列表、資訊頁、滑動換頁、功能列、活動看板、跑馬文字、即時通知訊息盒、使用者帳號、社群聯結<br>
                        ● 互動相關：投票、拍貼<br>
                        ● 其他(規劃中)：推播同步、內容更新與快取、第二螢同步<br>
                        </p>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <video class="videostyle" controls>
                            <source src="http://203.74.1.177/2TVnowdemo.mp4" type="video/mp4">
                        </video>
                    </div>
                </div>
            </div>

            <div class="row box-white">
                <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
                    <div class="col-md-4 col-xs-12">
                        <img src="images/metadata.png">
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <h3>電視節目metadata服務</h3>

                        <p>
                            資策會所開發的節目資料資料搜集和分析技術，24小時不間斷的搜集節目表(EPG)以及節目相關圖文資料(Metadata)，
                            將搜集到的資料以Web API的形式開放給開發者使用。
                            自動化的搜集與分析，可以提供最新的節目資料，以方便開發結合電視節目資料之應用，另外也可對電視導購商品連結做推薦參考。<br>
                            <br>
                            ● EPG電子節目表：
                            依指定日期提供使用者當日全天之節目表，或是目前每個頻道正在播放節目之資訊。<br>
                            ● 節目Metadata資料：
                            使用節目名稱取得節目相關metadata介紹資料，包含圖片，文字以及相關的影片。<br>
                        </p>
                    </div>
                </div>
            </div>

            <div class="row box-gray">
                <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
                    <div class="col-md-8 col-xs-12">
                        <h3>OTT多螢平台</h3>

                        <p>
                        OTT多螢影音平台是由資策會研發的一套系統，其目的在將客戶的原始視訊檔案轉換成不同解析度、不同傳輸速率的視訊檔案，
                        並將其上架到內容散佈網路(CDN)，提供各種不同尺寸螢幕的裝置(如PC、PAD、手機等)，在不同速率的網路下進行收視的服務。
                        客戶不需要自建機房，即可立即提供視訊服務，初期營運成本低，可提供與視訊服務緊密結合的互動活動，即時蒐集收視用戶的回饋，
                        適用於網路影音、線上電視購物、線上影音學習等業者。<br><br>
                        ● 自主開發可客製化的內容管理、營運管理系統與客戶端播放軟體<br>
                        ● 整合國外市佔率第一的轉檔軟體<br>
                        ● 整合支援全球播放的CDN服務<br>
                        ● 整合加值服務平台，提供與視訊服務緊密結合的互動服務與互動廣告，即時收集收視戶的反饋資訊<br>
                        ● DRM可依客戶指定廠家產品進行系統整合<br>
                        </p>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <img src="images/ott-1.png">
                    </div>
                </div>
            </div>
        </div>
        <!----//End-divice-features---->
        <!---start-footer---->
        <%@ include file="footer.jsp" %>
    </div>
</div>

<!--//End-content--->
<!----//End-wrap---->
</body>
</html>

