<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <title>2TVNow</title>
    <%@ include file="head.jsp" %>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-52625152-1', 'auto',{'name': '2tvnow'});
        ga('2tvnow.send', 'pageview');

    </script>

</head>

<body>
<div class="header">
    <%@ include file="menu.jsp" %>
    <!---//End-get-download-link---->
    <!----start-divice-features---->
    <div class="single-page">
        <div class="row box-gray">
            <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
                <div class="col-md-8 col-xs-12">
                    <h3>甚麼是2TV？</h3>

                    <p> 隨著行動裝置的出現，開始分散人們看電視的注意力。當小螢幕踏入一直由電視主宰的客廳時，將會帶來什麼改變？
                        處於新客廳的電視，扮演角色有何不同？當大小螢幕共存時，又該如何發揮互補力量？
                        週五晚上，一個屬於放鬆的時刻。你回到家打開電視，坐在沙發上準備享受正在熱播的偶像劇，順手拿起放在一旁的iPad，先打開信箱，
                        回了幾封下班前來不及處理的公事，接著打開購物網站，看看這周末的限時特價好康，順便連上Facebook， 和朋友分享今天發生的趣事。
                        就在進行這些事的當下，電視一直開著，你偶而抬頭關心一下劇情進度，思緒跟著情節起伏，但又不時地把目光移到iPad上。
                        這樣的情景你可能並不陌生，因為它每天都發生在你我的客廳中。現在它被賦予了一個專有 名詞─「第二螢幕」（Second Screen）。
                        這個形容或許非常貼切，指的是在觀賞第一螢幕「電視」的同時，與電視並行使 用的螢幕。在行動裝置興起後，第二螢幕就成為智慧型手機和平板電腦的代名詞。
                    </p>
                </div>
                <div class="col-md-4 col-xs-12">
                    <img src="images/2tvnow.png">
                </div>
            </div>
        </div>
        <div class="row box-white">
            <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
                <div class="col-md-4 col-xs-12">
                    <img src="images/2tvnow_2.png">
                </div>
                <div class="col-md-8 col-xs-12">
                    <h3>新科技戰場</h3>

                    <p>
                        第二螢幕之所以備受關注，一切都來自於「客廳」的戰略位置。從電視問世的那一刻起，客廳就一躍成為家裡的娛樂中心，不管其他消費產品如何推陳出新，
                        依然無法撼動客廳的地位，也讓客廳成為各大科技巨頭亟欲征服的戰場。根據尼爾森的調查，美國有86％的觀眾，會在看電視的同時使用平板電腦。這是一個
                        不容忽視的行為警訊，當行動裝置開始讓人們對電視的專注力降 低、使用行為也隨之多工時，若能在行動介面上推出和電視內容互相搭配的服務，就能從中挖
                        掘新的商機。這也是為什麼各項預 測直指，第二螢幕是2013年重要趨勢的原因之一。 電視大戰發展至此，首度加入了新的競爭者──行動App開發商。
                        雖然行動裝置瓜分不少使用者的注意力，但在大多情況下，做的 都是跟電視內容無關緊要的事，若能透過App帶給觀眾更好的觀賞體驗，
                        不僅能成功抓住這些四處漫遊的眼球，還能成為進軍家庭娛樂領域的跳板。瞬間，客廳這個小小空間又再起煙硝，成為各方人馬角力競逐的地方。
                        <br><br>
                    </p>
                </div>
            </div>
        </div>
        <div class="row box-gray">
            <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
                <div class="col-md-8 col-xs-12">
                    <h3>第二螢幕 讓看電視更有趣</h3>

                    <p> 第二螢幕不僅要讓第一螢幕變得更容易瀏覽，同時要將社會網絡整合至觀賞體驗裡，也要能無縫整合多種內容來源，再藉由提供延伸內容，
                        創造更深入且豐富的觀賞體驗，最後是讓觀眾找到他感興趣的新內容。未來平板電腦和智慧型手機的使用報告，都會開始納入與電視相關的活動數據。
                        像是在眾多頻道中快速找到想看內容的節目導航、一邊看電視一邊購買節目出現商品的電子商務、讓你與同好即時討論的社群互動、
                        馬上提供節目的延伸資訊、設計與節目相關的活動，如即時投票、劇情預測等。
                        只要內容夠吸引人、使用者對於第二螢幕的需求夠強烈，使用習慣一旦建立，第二螢幕的普及和更龐大商機，便是指日可待。
                        <br><br>
                    </p>
                </div>
                <div class="col-md-4 col-xs-12">
                    <img src="images/2tvnow-3.jpg">
                </div>
            </div>
        </div>
    </div>
    <!----//End-divice-features---->
    <!---start-footer---->
    <%@ include file="footer.jsp" %>
</div>
<!--//End-content--->
<!----//End-wrap---->
</body>
</html>

