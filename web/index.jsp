<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <title>2TVNow</title>
    <%@ include file="head.jsp" %>

    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-52625152-1', 'auto');
    ga('send', 'pageview');

    </script>

</head>
<body>
<!----start-wrap---->
<!----start-header----->
<div class="header">
    <%@ include file="menu.jsp" %>
    <!----//End-img-cursual---->
    <div class="col-md-8 col-md-offset-2 col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-lg-8 col-lg-offset-2">
        <div class="col-md-8 col-xs-12 index">
            <h3><span>科技進步</span> 行為改變</h3>

            <p>
                根據資策會FIND調查報告，目前有1/4的臺灣民眾(25.2%，約530萬)習慣在看電視時，同步使用智慧型手機，再加上電視數位化後，
                更引領了多屏跨界服務的應用以及生活型態的改變，隨著一雲多屏的應用興起，智慧/聯網電視的發展已成為當今數位智能家庭中最熱門的議題！
                近年來，手機、平板電腦等手持智慧裝置的普及、與使用各式服務App的興起，其實亦悄悄宣告第二螢創新與應用時化來臨，商機無限。
            </p>

            <h3><span>內容好壞</span> 成敗關鍵</h3>

            <p>全球市調機構調查均顯示，許多觀眾看電視的同時，會一邊使用智慧型手機、平板電腦等行動裝置，形成「多螢使用」現象，
                這現象也成為電視、網路服務新創意的發想點，當中的決勝點在於內容好壞。
                業者表示，亞洲此類「第二螢」操作最多的，當屬網路、內容產業正蓬勃發展的大陸，先前紅遍兩岸的知名節目「我是歌手」，
                由於節目紅、效果佳，屬經典案例。台灣結合多種螢幕的內容行銷，樂天並不是第一個案例。
            </p>

            <h3><span>創意點子</span> 創投青睞</h3>

            <p>「觀賞電視螢幕內容、用其他螢幕即時購物」的「第二屏」商機看好，另外一種在影片嵌入連結的數位「即看即買」技術，也有資金青睞。
            </p>

        </div>
        <div class="col-md-4 col-xs-12">
            <img src="images/2tvnow.png">
        </div>
    </div>
    <div class="clear"></div>
</div>
<!----//End-divice-features---->
<!---start-footer---->
<%@ include file="footer.jsp" %>
<!---//End-footer---->
<!--//End-content--->
<!----//End-wrap---->
</body>
</html>

