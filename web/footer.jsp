<%@ page contentType="text/html; charset=UTF-8" %>
<div class="share-icons">
    <div class="wrap">
        <img src="images/iii.png">

        <p>
            智慧網通系統研究所(簡稱智通所)原網路多媒體研究所，於2004年成立，發展願景為從客戶需求角度出發，以貼近使用者需求為導向。
            整合無線寬頻、智慧生活、能源資通訊、服務導向行動網路裝置、車載資資通訊等跨領域的核心技術，
            提出各項創新服務與應用以建置新一代的網路環境與智慧生活基礎設施，協助發展實現無所不在的優質智慧生活。
        </p>
    </div>
</div>
<footer>
    <div class="wrap">
        <div class="footer">
            <div class="footer-left">
                <p>&#169; 2016 <span>財團法人資訊工業策進會智慧網通系統研究所</span> 版權所有</p>
            </div>
            <div class="footer-right">
                <p>連絡 <a href="mailto:app@2tvnow.com">我們</a></p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</footer>