<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <title>2TVNow</title>
    <%@ include file="head.jsp" %>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-52625152-1', 'auto',{'name': 'forum'});
        ga('forum.send', 'pageview');

    </script>

</head>
<body>
<div class="header">
    <%@ include file="menu.jsp" %>
</div>
<!---//End-get-download-link---->
<!----start-divice-features---->
<div class="single-page">
    <div class="row box-gray">
        <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
            <div class="col-md-8 col-xs-12">
                <a href="https://www.facebook.com/groups/535760019852081/"><img src="images/fb.png" width="10%" alt=""
                                                                                class="img-left"></a>

                <h3>第二螢智慧電視產業論壇</h3>

                <p>
                    <br><br>
                    「第二螢智慧電視產業論壇」的舉辦是為了聚集產業相關業者以建立業界溝通平台以及聚集產業相關業者，
                    包含了內容創作及生產業者、Smart TV 業者、廣播及網路業者、廣告媒體業者、TV應用程式開發商等，
                    透過此一交流平台討論跨屏智慧電視之業界應用與互動問題，每次會邀請兩位國內相關產業的專家來分享。<br>
                    <a href="http://www.dtvc.org.tw/event/2015smarttv">智慧電視產業論壇</a><br><br>
                    「第二螢智慧電視產業論壇」定期交流活動會議：<br>
                    ● 時間：於每月最後一週星期二下午進行舉行<br>
                    ● 地點：資策會民生科技大樓 台北市松山區民生東路四段133號7樓<br>
                    <br>
                </p>
            </div>
            <div class="col-md-4 col-xs-12">
                <img src="images/confluence.png">
            </div>
            <!--<div class="col-md-8 col-xs-6">-->
            <!--<img src="images/confluence-2.jpg">-->
            <!--<img src="images/confluence-3.jpg">-->
            <!--<img src="images/confluence-4.jpg">-->
            <!--</div>-->
            <div class="col-md-4 col-xs-12">
                <img src="images/confluence-2.jpg">
            </div>
            <div class="col-md-4 col-xs-12">
                <img src="images/confluence-3.jpg">
            </div>
            <div class="col-md-4 col-xs-12">
                <img src="images/confluence-4.jpg">
            </div>
        </div>
    </div>
</div>
<!----//End-divice-features---->
<!---start-footer---->
<%@ include file="footer.jsp" %>
<!--//End-content--->
<!----//End-wrap---->
</body>
</html>

