<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <title>2TVNow</title>
    <%@ include file="head.jsp" %>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-52625152-1', 'auto',{'name': 'contact'});
        ga('contact.send', 'pageview');

    </script>

</head>
<body>
<div class="header">
    <%@ include file="menu.jsp" %>
    <!---//End-get-download-link---->
    <!----start-divice-features---->
    <div class="single-page">
        <div class="row box-gray">
            <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
                <div class="col-md-12 col-xs-12">
                    <h3>財團法人資訊工業策進會 智通所</h3>

                    <h2>105 台北市松山區民生東路四段133號7樓<br>
                        <br>
                        業務人員 - 顏彩純&nbsp; &nbsp; (02)6607-3898&nbsp;&nbsp;
                        <a href="mailto:hsu@iii.org.tw" style="color: #181881"> jessieyen@iii.org.tw</a>
                    </h2>

                    <p>
                        <br>
                        民生科技服務大樓位在民生東路四段與光復北路的交叉口，鄰近松山機場、西華飯店、民生圓環、介壽國中等，不論是開車前往或要利用大眾捷運交通系統都相當方便快速。<br>
                        ●自行開車：要自行開車前往的民眾，可利用大樓周邊的收費停車格，或者是位民生東路四段131巷內的資策會停車場(科技服務大樓正後方)，停車費為每小時30元，只接受悠遊卡繳費，請民眾停車前務必確認悠遊卡內的金額是否足夠。<br>
                        ●捷運：搭乘捷運的民眾請在松山機場站下車，再沿著光復北路往南步行約10~15分鐘後即可抵達。<br>
                        ●公車：搭乘公車的民眾，請利用 2、63、225、248、262、262 區、505、518、521、612、612 區、652、672、905、905
                        副、棕１等路線，在介壽國中站下車，斜對面便是本大樓。
                        <br><br>
                    </p>
                </div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3614.280629531608!2d121.55487240000005!3d25.0584759!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442abedc544a42d%3A0x6fcc945955e29634!2zMTA15Y-w5YyX5biC5p2-5bGx5Y2A5rCR55Sf5p2x6Lev5Zub5q61MTMz6Jmf!5e0!3m2!1szh-TW!2stw!4v1418277520962"
                        width="100%" height="600" frameborder="0" style="border:0"></iframe>
            </div>
        </div>
    </div>
    <!----//End-divice-features---->
    <!---start-footer---->

    <%@ include file="footer.jsp" %>
</div>
<!--//End-content--->
<!----//End-wrap---->
</body>
</html>

