<%@ page contentType="text/html; charset=UTF-8" %>
<header>
    <div class="wrap">
    <div class="header-left">
        <span> </span>
    </div>
    <div class="header-right">
        <span onclick="location.href='index.jsp';"> </span>
        <a class="logo" href="index.jsp"> <img src="images/logo.png" title="2tv"/></a>

        <p>第二螢電視互動新紀元</p>
        <!--<a class="bigbtn popup-with-zoom-anim" href="#small-dialog" href="#"> Download</a>-->
    </div>
    <div class="clear"></div>
</div>
    </div>
    <!----//End-header----->
    <!--start-content--->
    <div class="content">
        <div class="wrap">
            <!---pop-light-box--->

            <div id="owl-demo" class="owl-carousel">
                <div class="item" onclick="location.href='2tvnow.jsp';"><img class="lazyOwl" data-src="images/c1.png" alt="Lazy Owl Image">
                    <a href="#"><span>關於</span> 2TV</a>
                    <p>什麼是2TV？</p>
                </div>
                <div class="item" onclick="location.href='product.jsp';"><img class="lazyOwl" data-src="images/c2.png" alt="Lazy Owl Image">
                    <a href="#"><span>技術</span> 介紹</a>
                    <p>第二螢相關技術應用</p>
                </div>
                <div class="item" onclick="location.href='case.jsp';"><img class="lazyOwl" data-src="images/c3.png" alt="Lazy Owl Image">
                    <a href="#"><span>案例</span> 介紹</a>
                    <p>第二螢實際合作案例介紹</p>
                </div>
                <div class="item" onclick="location.href='news.jsp';"><img class="lazyOwl" data-src="images/c4.png" alt="Lazy Owl Image">
                    <a href="#"><span>最新</span> 消息</a>
                    <p>第二螢相關最新消息公佈</p>
                </div>
                <div class="item" onclick="location.href='forum.jsp';"><img class="lazyOwl" data-src="images/c5.png" alt="Lazy Owl Image">
                    <a href="#"><span>第二螢</span> 論壇</a>
                    <p>「第二螢智慧電視產業論壇」定期邀請專家經驗分享</p>
                </div>
                <div class="item" onclick="location.href='contact.jsp';"><img class="lazyOwl" data-src="images/c6.png" alt="Lazy Owl Image">
                    <a href="#"><span>連絡</span> 方式</a>
                    <p>歡迎和我們連絡</p>
                </div>
            </div>

            <marquee onMouseOver="this.stop()" onMouseOut="this.start()"><a href="http://www.dtvc.org.tw/event/2015smarttv/RE">第二螢智慧電視產業論壇，名額有限趕快報名喔！</a></marquee>

        </div>
    </div>

</header>