<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <title>2TVNow</title>
    <%@ include file="head.jsp" %>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-52625152-1', 'auto',{'name': 'case'});
        ga('case.send', 'pageview');

    </script>

</head>

<body>
<script>
    (function () {
        var list = [
            {
                "title": "",
                "content": "",
                "img": "",
                "video": "",
                "iphone": "",
                "google": ""
            },
            {
                "title": "",
                "content": "",
                "img": "",
                "video": "",
                "iphone": "",
                "google": ""
            }
        ]
    })();
</script>
<div class="header">
    <%@ include file="menu.jsp" %>

    <div id="small-dialog" class="mfp-hide small-dialog">
        <div class="pop_up">
            <h3>下載</h3>

            <p>趕快下載金鐘獎2014APP來操作看看吧！</p>
            <ul>
                <li><a class="iphone" href="https://itunes.apple.com/us/app/jin-zhong-jiang2014/id916063123?mt=8"> </a>
                </li>
                <li><a class="google" href="https://play.google.com/store/apps/details?id=com.totvnow.goldenbell"> </a>
                </li>
            </ul>
        </div>
    </div>

    <div id="small-dialog2" class="mfp-hide small-dialog">
        <div class="pop_up">
            <h3>下載</h3>

            <p>趕快下載樂購生活APP來操作看看吧！</p>
            <ul>
                <li><a class="iphone"
                       href="https://itunes.apple.com/tw/app/le-gou-sen-huo-shou-kan-dian/id906586411?mt=8"> </a></li>
                <li><a class="google"
                       href="https://play.google.com/store/apps/details?id=tw.com.umall.secondscreen"> </a></li>
            </ul>
        </div>
    </div>

    <div id="small-dialog3" class="mfp-hide small-dialog">
        <div class="pop_up">
            <h3>下載</h3>

            <p>趕快下載王子的約會APP來操作看看吧！</p>
            <ul>
                <li><a class="google"
                       href="https://play.google.com/store/apps/details?id=com.ttv.takemeouttw&hl=zh_TW"> </a></li>
            </ul>
        </div>
    </div>

    <div id="small-dialog4" class="mfp-hide small-dialog">
        <div class="pop_up">
            <h3>下載</h3>

            <p>趕快下載Juiker 揪科APP來操作看看吧！</p>
            <ul>
                <li><a class="iphone" href="https://itunes.apple.com/tw/app/juiker/id527138436?l=zh&mt=8"> </a></li>
                <li><a class="google" href="https://play.google.com/store/apps/details?id=org.itri&hl=zh_TW"> </a></li>
            </ul>
        </div>
    </div>

    <div id="small-dialog5" class="mfp-hide small-dialog">
        <div class="pop_up">
            <h3>下載</h3>

            <p>趕快下載金曲獎APP來體驗不一樣的收視趣味！</p>
            <ul>
                <li><a class="iphone" href="https://itunes.apple.com/us/app/jin-qu26/id990934598?l=zh&ls=1&mt=8"> </a>
                </li>
                <li><a class="google"
                       href="https://play.google.com/store/apps/details?id=com.golden.melody.twentysix"> </a></li>
            </ul>
        </div>
    </div>

    <div id="small-dialog6" class="mfp-hide small-dialog">
        <div class="pop_up">
            <h3>下載</h3>

            <p>趕快下載台視新聞App瀏覽最新的新聞資訊吧！</p>
            <ul>
                <li><a class="google"
                       href="https://play.google.com/store/apps/details?id=com.totvnow.ttv&hl=zh_TW"> </a></li>
            </ul>
        </div>
    </div>

    <div id="small-dialog7" class="mfp-hide small-dialog">
        <div class="pop_up">
            <h3>下載</h3>

            <p>趕快下載一直OnTv玩玩看吧！</p>
            <ul>
                <li><a class="iphone" href="https://itunes.apple.com/app/id966810851"> </a></li>
                <li><a class="google" href="https://play.google.com/store/apps/details?id=com.ctv.ctv2tvnow"> </a></li>
            </ul>
        </div>
    </div>

    <div id="small-dialog8" class="mfp-hide small-dialog">
        <div class="pop_up">
            <h3>下載</h3>

            <p>趕快下載 金鐘50 參加彩金搖搖樂拿大獎！</p>
            <ul>
                <li><a class="iphone" href="https://itunes.apple.com/app/id966810851"> </a></li>
                <li><a class="google" href="https://play.google.com/store/apps/details?id=com.ctv.ctv2tvnow"> </a></li>
            </ul>
        </div>
    </div>

    <div id="small-dialog9" class="mfp-hide small-dialog">
        <div class="pop_up">
            <h3>下載</h3>

            <p>趕快下載 衛視電影台暑假打卡活動APP來操作看看吧！</p>
            <ul>
                <li><a class="iphone"
                       href="https://itunes.apple.com/tw/app/timely.tv-tai-wan-dian-shi/id514555804?l=zh&mt=8"> </a>
                </li>
                <li><a class="google" href="https://play.google.com/store/apps/details?id=com.sixnology.timely"> </a>
                </li>
            </ul>
        </div>
    </div>

</div>
<!---//End-get-download-link---->
<!----start-divice-features---->
<div class="single-page">
    <div class="row box-white">
        <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
            <div class="col-md-4 col-xs-12">
                <br>
                <img src="images/timely0813.png">
            </div>
            <div class="col-md-8 col-xs-12 marginbr">
                <h3>國家地理頻道《台灣菁英戰士：傲氣飛鷹》</h3>

                <p>
                    與索驥Timely.tv APP及國家地理頻道合作，運用ACR技術進行電視打卡的第二螢互動<br>
                    8/13起每週四晚上10點收看國家地理頻道《台灣菁英戰士：傲氣飛鷹》，開啟Timely.tv
                    App進入「台灣菁英戰士：傲氣飛鷹」活動專區，聽電視打卡回答當集節目問題，就有機會抽中藍牙無線揚聲器、隨身印相機，週週集點最後還有CITIZEN手錶等著你！
                    <br><br>
                </p>
                <a class="bigbtn popup-with-zoom-anim" href="#small-dialog9" href="#">下載</a>
            </div>
        </div>
    </div>

    <div class="row box-gray">
        <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
            <div class="col-md-8 col-xs-12 marginbr">
                <h3>緯來戲劇台「天國的眼淚」</h3>

                <p>
                    與索驥Timely.tv APP及緯來戲劇台合作，運用ACR技術進行電視打卡的第二螢互動<br>
                    5/18起週一到週五每晚10點收看緯來戲劇台「天國的眼淚」，開啟Timely.tv App聽電視打卡回答劇情問題，打卡5次集滿5滴眼淚即可參加抽獎，蒐集越多中獎機會越大！
                    <br><br>
                </p>
                <a class="bigbtn popup-with-zoom-anim" href="#small-dialog9" href="#">下載</a>
            </div>
            <div class="col-md-4 col-xs-12">
                <img src="images/timely0518.png">
            </div>
        </div>
    </div>

    <div class="row box-white">
        <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
            <div class="col-md-4 col-xs-12">
                <br>
                <img src="images/timely0225.png">
            </div>
            <div class="col-md-8 col-xs-12 marginbr">
                <h3>緯來戲劇台 「奇妙一家人」</h3>

                <p>
                    與索驥Timely.tv APP及緯來戲劇台合作，運用ACR技術進行電視打卡的第二螢互動<br>
                    2/25起於《奇妙一家人》播出時，聽電視打卡回答劇情問題，答對前500名可獲得咖啡兌換券，打卡集滿三個徽章即可參加抽獎，蒐集徽章愈多，中獎機率愈高！
                    <br><br>
                </p>
                <a class="bigbtn popup-with-zoom-anim" href="#small-dialog9" href="#">下載</a>
            </div>
        </div>
    </div>

    <div class="row box-gray">
        <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
            <div class="col-md-8 col-xs-12 marginbr">
                <h3>「I Like NAT GEO」電視打卡活動</h3>

                <p>
                    與索驥Timely.tv APP合作，運用ACR技術進行電視打卡的第二螢互動<br>
                    觀眾只要在《I Like NAT GEO》系列節目播出時，打開並進入Timely.tv
                    APP內的活動專區，聽電視打卡回答問題即可獲得一枚徽章參加抽獎，每天都有機會得到高級藍芽喇叭、高級耳罩式耳機、行動電源及國家地理頻道連帽外套等多項好禮。
                    <br><br>
                </p>
                <a class="bigbtn popup-with-zoom-anim" href="#small-dialog9" href="#">下載</a>
            </div>
            <div class="col-md-4 col-xs-12">
                <img src="images/timely1222.png">
            </div>
        </div>
    </div>

    <div class="row box-white">
        <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
            <div class="col-md-4 col-xs-12">
                <br>
                <img src="images/timely2.png">
            </div>
            <div class="col-md-8 col-xs-12 marginbr">
                <h3>緯來夏日好集打卡活動</h3>

                <p>
                    與索驥Timely.tv APP及衛視電影台合作，運用ACR技術進行電視打卡的第二螢互動<br>
                    【緯來夏日好集】電視打卡集字抽好禮！7/25起鎖定緯來家族頻道指定節目首播時段，聽電視打卡集字即可抽iPad、PS4、DONNAFIFI女用長夾、時尚休閒背包、Too Cool For
                    School美妝優惠券等好禮（緯來精采台點數3倍）！蒐集6點可集1個字，越多朋友參加，點數越多哦！
                    <br><br>
                </p>
                <a class="bigbtn popup-with-zoom-anim" href="#small-dialog9" href="#">下載</a>
            </div>
        </div>
    </div>

    <div class="row box-gray">
        <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
            <div class="col-md-8 col-xs-12 marginbr">
                <h3>衛視電影台暑假打卡活動</h3>

                <p>
                    與索驥Timely.tv APP及衛視電影台合作，運用ACR技術進行電視打卡的第二螢互動<br>
                    7、8月每週六晚上9點鎖定衛視電影台，電影廣告時間聽電視打卡進入「影癡小學堂」問答遊戲，就有機會抽Apple Watch、GoPro運動攝影機等好禮！更多資訊請上衛視電影台臉書粉絲團查詢。
                    <br><br>
                </p>
                <a class="bigbtn popup-with-zoom-anim" href="#small-dialog9" href="#">下載</a>
            </div>
            <div class="col-md-4 col-xs-12">
                <img src="images/timely.png">
            </div>
        </div>

    </div>


    <div class="row box-white">
        <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
            <div class="col-md-4 col-xs-12">
                <video class="videostyle" controls>
                    <source src="http://203.74.1.177/goldenbell2015.mp4" type="video/mp4">
                </video>
            </div>
            <div class="col-md-8 col-xs-12 marginbr">
                <h3>第50屆金鐘獎頒獎典禮</h3>

                <p>
                    參加金鐘50官方APP「彩金搖搖樂」活動，就有機會獲得獎金大獎，最高獎金新台幣6萬元，
                    還有機會抽中頒獎典禮門票，前進典禮現場，共享金鐘榮耀！
                    同時金鐘50 app中的技術還入圍美國 R&D 100 百大科技研發獎！
                    <br><br>
                </p>
                <a class="bigbtn popup-with-zoom-anim" href="#small-dialog8" href="#">下載</a>
            </div>
        </div>
    </div>


    <div class="row box-gray">
        <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
            <div class="col-md-8 col-xs-12 marginbr">
                <h3>中視 一直OnTv</h3>

                <p>
                    《ON TV》是一個生活直播的App，提供分類新聞、參加活動抽好禮，熱門巨星演唱會等節目。
                    2T團隊在App中加上了ACR的技術，讓使用者可以擁有不一樣的體驗<br><br>
                </p>
                <a class="bigbtn popup-with-zoom-anim" href="#small-dialog7" href="#">下載</a>
            </div>
            <div class="col-md-4 col-xs-12">
                <img src="images/ctv.jpg">
            </div>
        </div>
    </div>

    <div class="row box-white">
        <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
            <div class="col-md-4 col-xs-12">
                <br>
                <img src="images/ttvnews.jpg">
            </div>
            <div class="col-md-8 col-xs-12 marginbr">
                <h3>台視新聞</h3>

                <p>
                    2015年1月1日起，台視新聞台正式開台，台視新聞邁向全新聞頻道，24小時不間斷提供高畫質的即時新聞及優質豐富的新聞節目，
                    以服務更廣大的收視觀眾，同時台視也委託2TV團隊開發“台視新聞”App，讓台視新聞繼續陪伴您成長，記錄台灣和世界的現在與未來。
                    <br><br>
                </p>
                <a class="bigbtn popup-with-zoom-anim" href="#small-dialog6" href="#">下載</a>
            </div>
        </div>
    </div>


    <div class="single-page">
        <div class="row box-gray">
            <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
                <div class="col-md-8 col-xs-12 marginbr">
                    <h3>2015年 金曲26</h3>

                    <p>
                        金曲獎是台灣規模最大的音樂獎，也是華人世界中兼具榮譽和影響力的音樂獎勵活動，
                        與金馬獎、金鐘獎並稱為台灣三大娛樂獎。
                        今年首次在app中加入ACR自動內容辨的技術，讓觀眾擁有不一樣的收視體驗。<br><br>
                    </p>
                    <a class="bigbtn popup-with-zoom-anim" href="#small-dialog5" href="#">下載</a>
                </div>
                <div class="col-md-4 col-xs-12">
                    <br>
                    <img src="images/gma.jpg">

                </div>
            </div>
        </div>


        <div class="row box-white">
            <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
                <div class="col-md-4 col-xs-12">
                    <video class="videostyle" controls>
                        <source src="http://203.74.1.177/HBL.mp4" type="video/mp4">
                    </video>
                </div>
                <div class="col-md-8 col-xs-12 marginbr">
                    <h3>Fox體育台零秒出手</h3>

                    <p>
                        台灣第一支互動投籃的創意影音活動！看電視也能練投籃~~精準出手，還能抽價值萬元的PS4與SONY運動攝影機!
                        <br><br>
                    </p>
                </div>
            </div>
        </div>

        <div class="single-page">
            <div class="row box-gray">
                <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
                    <div class="col-md-8 col-xs-12 marginbr">
                        <h3>王子的約會</h3>

                        <p>
                            全球超過15個國家，美國、澳洲、荷蘭、愛爾蘭、丹麥、日本、韓國、泰國、西班牙、中國、印尼、菲律賓 …<br>
                            在各國皆掀起交友節目風潮及創新收視。<br>
                            資策會和台灣電視公司的知名節目「王子的約會」合作，開發出電視節目的第二螢互動APP，
                            <br><br>
                        </p>
                        <a class="bigbtn popup-with-zoom-anim" href="#small-dialog3" href="#">下載</a><br>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <br>
                        <video class="videostyle" controls>
                            <source src="http://203.74.1.177/ttv.mp4" type="video/mp4">
                        </video>
                    </div>
                </div>
            </div>

            <div class="row box-white">
                <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
                    <div class="col-md-4 col-xs-12">
                        <video class="videostyle" controls>
                            <source src="http://vod.watch.juiker.tw/0/102getfresh/2013getfresh.mp4" type="video/mp4">
                        </video>
                    </div>
                    <div class="col-md-8 col-xs-12 marginbr">
                        <h3>搶鮮大賽頒獎典禮</h3>

                        <p>

                            2013搶鮮大賽頒獎典禮、2013產創獎典禮、2014搶鮮大賽頒獎典禮，工研院的Juiker Watch影音服務平台結合資策會的2TVNow推播系統，
                            於Web、iOS、Android三大平台提供應用服務，透過網路同步觀看頒獎典禮視訊實況並即時接收相關訊息，包含目前頒發獎項、得獎者與得獎內容說明等。<br><br>
                        </p>
                        <a class="bigbtn popup-with-zoom-anim" href="#small-dialog4" href="#">下載</a>
                    </div>
                </div>
            </div>


            <div class="row box-gray">
                <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
                    <div class="col-md-8 col-xs-12">
                        <h3>超視-頂尖對決</h3>

                        <p>
                            與索驥Timely.tv APP及超視合作，運用ACR技術進行電視打卡的第二螢互動<br>
                            看超視頂尖對決300份咖啡冰淇淋免費送給你<br>
                            活動辦法：於指定快閃時段，開啟APP聽取《頂尖對決》節目播出聲音分享或蒐集3張拼圖，300份咖啡冰淇淋免費送！
                            <br>
                        </p>
                        <a class="bigbtn popup-with-zoom-anim" href="#small-dialog9" href="#">下載</a>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <img src="images/stv-1.jpg">
                    </div>
                </div>
            </div>


            <div class="row box-white">
                <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
                    <div class="col-md-4 col-xs-12">
                        <video class="videostyle" controls>
                            <source src="http://203.74.1.177/videoland.mp4" type="video/mp4">
                        </video>
                    </div>
                    <div class="col-md-8 col-xs-12 marginbr">
                        <h3>緯來戲劇台-王牌大明星</h3>

                        <p>
                            與索驥Timely.tv APP及緯來戲劇台合作，運用ACR技術進行電視打卡的第二螢互動<br>
                            晚間8-10點打開app進入活動專區，以手機靠近電視打卡成功即可蒐集一枚徽章，集滿三枚徽章就可兌換7-11抵用券！
                            <br><br>
                        </p>
                        <a class="bigbtn popup-with-zoom-anim" href="#small-dialog9" href="#">下載</a>
                    </div>
                </div>
            </div>

            <div class="row box-gray">
                <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1">
                    <div class="col-md-8 col-xs-12 marginbr">
                        <h3>2014年第49屆金鐘獎頒獎典禮</h3>

                        <p>
                            「金鐘獎2014」是個整合了各種你觀看金鐘獎時所需功能的便利App。它的功能包括：<br>
                            <br>
                            ● 金鐘獎相關新聞，以及金鐘獎歷史<br>
                            ● 金鐘獎入圍名單<br>
                            ● 各種金鐘獎相關影片<br>
                            ● 金鐘獎直播<br>
                            ● 金鐘獎相關各種投票活動<br>
                            ● 將新聞或入圍名單分享至Facebook<br><br>
                        </p>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <br><br>
                        <video class="videostyle" controls>
                            <source src="http://203.74.1.177/goldenbell.mp4" type="video/mp4">
                        </video>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!----//End-divice-features---->
    <!---start-footer---->
    <%@ include file="footer.jsp" %>
    <!--//End-content--->
    <!----//End-wrap---->
</body>
</html>

