<%@ page contentType="text/html; charset=UTF-8" %>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=7; IE=8; IE=9">
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>
<script type="application/x-javascript">
    addEventListener("load", function () {
        setTimeout(hideURLbar, 0);
    }, false);
    function hideURLbar() {
        window.scrollTo(0, 1);
    }
</script>
<!----webfonts---->
<link href="css/bootstrap/bootstrap.min.css" rel="stylesheet">
<link href="css/font.css" rel="stylesheet">
<link href="css/style.css" rel='stylesheet' type='text/css'/>
<!--< !----//webfonts--&ndash;&gt;-->
<!-- Owl Carousel Assets -->
<link href="css/owl.carousel.css" rel="stylesheet">
<script src="js/jquery-1.9.1.min.js"></script>
<!-- Owl Carousel Assets -->
<!-- Prettify -->
<script src="js/owl.carousel.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/angular/angular.min-1.2.21.js"></script>
<script>
    $(document).ready(function () {

        $("#owl-demo").owlCarousel({
            items: 4,
            lazyLoad: true,
            autoPlay: true,
            navigation: true,
            navigationText: ["", ""],
            rewindNav: false,
            scrollPerPage: false,
            pagination: false,
            paginationNumbers: false
        });

    });
</script>
<!-- //Owl Carousel Assets -->
<!-- Add fancyBox light-box -->
<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
<link href="css/magnific-popup.css" rel="stylesheet" type="text/css">
<script>
    $(document).ready(function () {
        $('.popup-with-zoom-anim').magnificPopup({
            type: 'inline',
            fixedContentPos: false,
            fixedBgPos: true,
            overflowY: 'auto',
            closeBtnInside: true,
            preloader: false,
            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in'
        });
    });
</script>
<!-- //End fancyBox light-box -->
